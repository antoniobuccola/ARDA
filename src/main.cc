#include <iostream>
#include "cRunFinder.h"
#include "cUserInput.h"
#include "cLookupTable.h"
#include <experimental/filesystem>
#include <climits>
#include <string>
#include <stdexcept>

using namespace std;
namespace fs = std::experimental::filesystem;

int main(int argc, char const *argv[]) {
  cUserInput::setUserInput(argc, argv);

  if (!cUserInput::isSet("nobanner")){
    #include "banner.h"
  }

  if (cUserInput::getArgs("tempdir") != "" &&
      cUserInput::getArgs("rundir")  != "" &&
      cUserInput::getArgs("outdir")  != "") {

    if (!fs::exists(fs::path(cUserInput::getArgs("tempdir")))) {
      cout << "Temporary directory " << cUserInput::getArgs("tempdir") << " not found!" << endl;
      return 0;
    }

    if (!fs::exists(fs::path(cUserInput::getArgs("outdir")))) {
      cout << "Output directory " << cUserInput::getArgs("outdir") << " not found!" << endl;
      return 0;
    }

    if (!fs::exists(fs::path(cUserInput::getArgs("rundir")))) {
      cout << "Run directory " << cUserInput::getArgs("rundir") << " not found!" << endl;
      return 0;
    }

    int nstart;
    int nstop;

    if (cUserInput::getArgs("start") != "" && cUserInput::getArgs("stop") != "") {
      try {
        nstart = stoi(cUserInput::getArgs("start"));
        nstop  = stoi(cUserInput::getArgs("stop"));
      }
      catch (...) {
        cout << "Invalid start stop numbers" << endl;
        return 0;
      }
    } else {
      cout << "No start stop numbers" << endl;
      return 0;
    }

    cout << "RUNNING ANALYSIS" << endl;

    Int_t nFrameToRead;
    try {
      nFrameToRead = stoi(cUserInput::getArgs("limit"));
    } catch (invalid_argument e) {
      nFrameToRead = -1;
    }

    cRunFinder rf(nstart, nstop, cUserInput::getArgs("rundir"), cUserInput::getArgs("tempdir"), cUserInput::getArgs("outdir"), nFrameToRead);
    rf.initializeLookupTable();
    rf.findRuns();
    rf.threadAnalyse();
  }

  return 0;
}
