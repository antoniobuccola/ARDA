#ifdef __CINT__
#pragma link C++ class cHit+;
#pragma link C++ class cRawEvent+;
#pragma link C++ class cLookupTable+;
#pragma link C++ class cRawSignal+;
#endif

#ifdef _MAKECINT_
#pragma link C++ class vector<cHit>+;
#pragma link C++ class map<Int_t, cLookupTable::chanData>+;
#pragma link C++ class vector<cRawSignal>;
#endif
